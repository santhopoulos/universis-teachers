# UniverSIS
[UniverSIS](https://universis.gr) is a coordinated effort by Greek academic institutions to build a Student Information System as an open source platform. The target is to serve our common needs to support academic and administrative processes.

## UniverSIS-teachers
Teacher application features

* Teacher dashboard for current courses/exams
* Teacher personal profile info
* Semester classes  and previous teaching assignments
* Management of current class, class profile, students, grades
* Thesis info

## Demo
UniverSIS teachers demo application is available here: https://teachers.universis.io

You can use the following credentials to test various cases:

* teacher1@example.com

E8F64109-18FF-47FD-A370-96024CF8A94D

* teacher2@example.com

552D2ED8-C352-4B62-A637-4B6FB6FF5372

(with course class sections at “Μηχανική ΙΙΙ”)

* teacher3@example.com

7B2FB575-C01E-40AC-B011-0CC51D64D74A

## Installation
  Installation instructions can be found in the [installation file](INSTALL.md)

## How to contribute

We use Gitlab to track bugs and feature requests for the code. When submitting a bug please make sure you submit a comprehensive description and all relevant information.

## Contact
If you are interested in participating in the development efforts, please contact us at **info@universis.gr**

## Questions?

Check our wiki [FAQ](https://gitlab.com/universis/universis.io/wikis/FAQ) page, and feel free to [contact us](https://www.universis.gr/#contact).
