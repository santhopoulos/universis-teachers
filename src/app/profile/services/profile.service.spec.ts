import { TestBed } from '@angular/core/testing';

import { ProfileService } from './profile.service';
import { MostModule } from '@themost/angular';

describe('ProfileService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports:[
      MostModule.forRoot({
        base: '/',
        options: {
            useMediaTypeExtensions: false
        }
      })
    ],
    providers: [ProfileService]
  }));

  it('should be created', () => {
    const service: ProfileService = TestBed.get(ProfileService);
    expect(service).toBeTruthy();
  });
});
