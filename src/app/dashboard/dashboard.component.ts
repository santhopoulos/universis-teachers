import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {Model} from '@universis/ngx-events';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent {

  model = Model.Instructor;

  constructor() { }

}
