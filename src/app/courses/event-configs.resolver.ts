import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable, of} from "rxjs";
import {AngularDataContext} from "@themost/angular";
import {ProfileService} from "../profile/services/profile.service";

export class EventAbsencesResolver implements Resolve<any>{
  constructor(private _context: AngularDataContext) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    if (route.parent && route.parent.params && route.params) {
      return this._context.model('instructors/me/classes')
        .where('course').equal(route.parent.params.course)
        .and('year').equal(route.parent.params.year)
        .and('period').equal(route.parent.params.period)
        .select('id')
        .getItem().then(x => {
          const courseClass = x;
          return (`instructors/me/classes/${x.id}/teachingEvents/${route.params.id}/attendance`)
      });
    }
    return undefined;
  }
}

export class EventConfigResolver implements Resolve<any>{
  constructor(private _context: AngularDataContext) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    if (route.parent && route.parent.params){
      return this._context.model('instructors/me/classes')
        .where('course').equal(route.parent.params.course)
        .and('year').equal(route.parent.params.year)
        .and('period').equal(route.parent.params.period)
        .select('id')
        .getItem().then(x => {
          return (`instructors/me/classes/${x.id}/teachingEvents`)
        });
    }
    return undefined;
  }
}

export class EventCourseClassResolver implements Resolve<any>{
  constructor(private _context: AngularDataContext) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    if (route.parent && route.parent.params){
      return this._context.model('instructors/me/classes')
        .where('course').equal(route.parent.params.course)
        .and('year').equal(route.parent.params.year)
        .and('period').equal(route.parent.params.period)
        .expand('course')
        .getItem();
    }
    return undefined;
  }
}

export class EventCourseClassInstructorResolver implements Resolve<any>{
  constructor(private _profileService: ProfileService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this._profileService.getInstructor().catch( () => {
      return undefined;
    });

  }
}

export class EventCourseClassSectionEndpointResolver implements Resolve<any>{
  constructor(private _context: AngularDataContext) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    if (route.parent && route.parent.params){
      return this._context.model('instructors/me/classes')
        .where('course').equal(route.parent.params.course)
        .and('year').equal(route.parent.params.year)
        .and('period').equal(route.parent.params.period)
        .select('id')
        .getItem().then(x => {
          return (`instructors/me/classes/${x.id}/sections`)
        });
    }

  }
}

export class ShowActionButtonResolver implements Resolve<any>{
  constructor(private _context: AngularDataContext) {
  }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    const showActions = false;
    return showActions
  }
}

export class EventModelResolver implements Resolve<any>{

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    if (route.parent && route.parent.params && route.params) {
      return of('instructors/me/teachingEvents');
    }
    return undefined;
  }
}

export class DeleteEventModelResolver implements Resolve<any> {

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    if(route.parent && route.parent.params && route.params) {
      return of(`instructors/me/teachingEvents/${route.params.id}`)
    }
    return undefined;
  }
}
