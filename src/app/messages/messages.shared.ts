import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {FormsModule} from '@angular/forms';
import { SendMessageToStudentComponent } from './components/send-message-to-student/send-message-to-student.component';
import {SendMessageToClassComponent} from "./components/send-message-to-class/send-message-to-class.component";
import {SendMessageToExamParticipantsComponent} from "./components/send-message-to-exam-participants/send-message-to-exam-participants.component";

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule
  ],
  declarations: [
    SendMessageToClassComponent,
    SendMessageToExamParticipantsComponent,
    SendMessageToStudentComponent
  ],
  exports: [
    SendMessageToStudentComponent,
    SendMessageToClassComponent,
    SendMessageToExamParticipantsComponent
  ],
  providers: [

  ],
  entryComponents: [
    SendMessageToExamParticipantsComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MessagesSharedModule implements OnInit {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading messages shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/messages.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }

}
