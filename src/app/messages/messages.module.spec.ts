import { MessagesModule } from './messages.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule , TranslateService} from '@ngx-translate/core';
import {MessagesRouting} from './messages.routing';
import { MessagesSharedModule } from './messages.shared';
import { FormsModule } from '@angular/forms';
import { MessagesHomeComponent } from './components/messages-home/messages-home.component';
import { MessagesListComponent } from './components/messages-list/messages-list.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {inject, TestBed} from '@angular/core/testing';

describe('MessagesModule', () => {
  beforeEach(() => {
    return TestBed.configureTestingModule({
      imports: [
        CommonModule,
        TranslateModule.forRoot(),
        MessagesRouting,
        MessagesSharedModule,
        FormsModule,
        InfiniteScrollModule
      ],
      declarations: [MessagesHomeComponent, MessagesListComponent]
    }).compileComponents();
  });

  it('should create an instance', inject([TranslateService], (translateService: TranslateService) => {
    const messagesModule = new MessagesModule(translateService);
    expect(messagesModule).toBeTruthy();
  }));
});
