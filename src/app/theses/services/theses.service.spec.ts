import { TestBed } from '@angular/core/testing';

import { ThesesService } from './theses.service';
import { MostModule } from '@themost/angular';

describe('ThesesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      MostModule.forRoot({
        base: '/',
        options: {
            useMediaTypeExtensions: false
        }
      })
    ]
  }));

  it('should be created', () => {
    const service: ThesesService = TestBed.get(ThesesService);
    expect(service).toBeTruthy();
  });
});
